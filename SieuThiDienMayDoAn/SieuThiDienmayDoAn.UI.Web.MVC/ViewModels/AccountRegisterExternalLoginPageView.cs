﻿using SieuThiDienMayDoAn.Controllers.ViewModels.ProductCatalog;
using SieuThiDienMayDoAn.UI.Web.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SieuThiDienmayDoAn.UI.Web.MVC.ViewModels
{
    public class AccountRegisterExternalLoginPageView : BaseProductCatalogPageView
    {
        public RegisterExternalLoginModel RegisterExternalLoginModel { get; set; }
    }
}