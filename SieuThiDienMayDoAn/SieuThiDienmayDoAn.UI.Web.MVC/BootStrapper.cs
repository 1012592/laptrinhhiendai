﻿using StructureMap;
using StructureMap.Configuration.DSL;
using SieuThiDienMayDoAn.Infrastructure.Configuration;
using SieuThiDienMayDoAn.Infrastructure.Email;
using SieuThiDienMayDoAn.Infrastructure.Logging;
using SieuThiDienMayDoAn.Infrastructure.UnitOfWork;
using SieuThiDienMayDoAn.Model.Categories;
using SieuThiDienMayDoAn.Model.Products;
using SieuThiDienMayDoAn.Services.Implementations;
using SieuThiDienMayDoAn.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SieuThiDienMayDoAn.Model.Brands;
using SieuThiDienMayDoAn.Model.Suppliers;
using SieuThiDienMayDoAn.Model.SuperCategories;
using SieuThiDienMayDoAn.Model.Basket;
using SieuThiDienMayDoAn.Model.Shipping;
using SieuThiDienMayDoAn.Infrastructure.CookieStorage;
using SieuThiDienMayDoAn.Model.Review;
using SieuThiDienMayDoAn.Model.Customers;
using SieuThiDienMayDoAn.Model.Orders;


namespace SieuThiDienMayDoAn.UI.Web.MVC
{
    public class BootStrapper
    {
        public static void ConfigureDependencies()
        {
            ObjectFactory.Initialize(x =>
            {
                x.AddRegistry<ControllerRegistry>();
            });
        }

        public class ControllerRegistry : Registry
        {
            public ControllerRegistry()
            {
                // Repositories
                ForRequestedType<ICategoryRepository>().TheDefault.Is.OfConcreteType
                    <SieuThiDienMayDoAn.Repository.NHibernate.Repositories.CategoryRepository>();

                

                ForRequestedType<IProductRepository>().TheDefault.Is.OfConcreteType
                    <SieuThiDienMayDoAn.Repository.NHibernate.Repositories.ProductRepository>();


                ForRequestedType<IBrandRepository>().TheDefault.Is.OfConcreteType
                    <SieuThiDienMayDoAn.Repository.NHibernate.Repositories.BrandRepository>();



                ForRequestedType<ISupplierRepository>().TheDefault.Is.OfConcreteType
                    <SieuThiDienMayDoAn.Repository.NHibernate.Repositories.SupplierRepository>();

                ForRequestedType<IReviewRepository>().TheDefault.Is.OfConcreteType
                    <SieuThiDienMayDoAn.Repository.NHibernate.Repositories.ReviewRepository>();



                ForRequestedType<IRatingRepository>().TheDefault.Is.OfConcreteType
                    <SieuThiDienMayDoAn.Repository.NHibernate.Repositories.RatingRepository>();


                ForRequestedType<ISuperCategoryRepository>().TheDefault.Is.OfConcreteType
                    <SieuThiDienMayDoAn.Repository.NHibernate.Repositories.SuperCategoryRepository>();

                ForRequestedType<ICustomerRepository>().TheDefault.Is.OfConcreteType
                    <SieuThiDienMayDoAn.Repository.NHibernate.Repositories.CustomerRepository>();

                ForRequestedType<IDeliveryAddressRepository>().TheDefault.Is.OfConcreteType
                    <SieuThiDienMayDoAn.Repository.NHibernate.Repositories.DeliveryAddressRepository>();

                ForRequestedType<IDeliveryOptionRepository>().TheDefault.Is.OfConcreteType
                    <SieuThiDienMayDoAn.Repository.NHibernate.Repositories.DeliveryOptionRepository>();
                ForRequestedType<IOrderRepository>().TheDefault.Is.OfConcreteType
                    <SieuThiDienMayDoAn.Repository.NHibernate.Repositories.OrderRepository>();

                //Services
                
                ForRequestedType<IProductCatalogService>().TheDefault.Is.OfConcreteType
                    <ProductCatalogService>();
                ForRequestedType<IProductService>().TheDefault.Is.OfConcreteType
                    <ProductService>();

                ForRequestedType<ICategoryService>().TheDefault.Is.OfConcreteType
                   <CategoryService>();
                ForRequestedType<IBrandService>().TheDefault.Is.OfConcreteType
                    <BrandService>();
                ForRequestedType<ISupplierService>().TheDefault.Is.OfConcreteType
                   <SupplierService>();
                ForRequestedType<ISuperCategoryService>().TheDefault.Is.OfConcreteType
                    <SuperCategoryService>();

                ForRequestedType<IReviewRatingService>().TheDefault.Is.OfConcreteType
                    <ReviewRatingService>();
                ForRequestedType<ICustomerService>().TheDefault.Is.OfConcreteType
                   <CustomerService>();

                ForRequestedType<IShippingService>().TheDefault.Is.OfConcreteType
                   <ShippingService>();
                ForRequestedType<IOrderService>().TheDefault.Is.OfConcreteType
                  <OrderService>();

                ForRequestedType<IDeliveryAddressService>().TheDefault.Is.OfConcreteType
                 <DeliveryAddressService>();

                ForRequestedType<IUnitOfWork>().TheDefault.Is.OfConcreteType
                    <SieuThiDienMayDoAn.Repository.NHibernate.NHUnitOfWork>();

                

                // Application Settings
                ForRequestedType<IApplicationSettings>().TheDefault.Is.OfConcreteType
                    <WebConfigApplicationSettings>();
                // Logger
                ForRequestedType<ILogger>().TheDefault.Is.OfConcreteType
                    <Log4NetAdapter>();
                // E-Mail Service
                ForRequestedType<IEmailService>().TheDefault.Is.OfConcreteType
                    <TextLoggingEmailService>();

                ForRequestedType<IBasketRepository>().TheDefault.Is.OfConcreteType
                    <Repository.NHibernate.Repositories.BasketRepository>();
                ForRequestedType<IDeliveryOptionRepository>().TheDefault.Is.
                    OfConcreteType<Repository.NHibernate.Repositories.DeliveryOptionRepository>();

                ForRequestedType<IBasketService>().TheDefault.Is.OfConcreteType<BasketService>();

                ForRequestedType<ICookieStorageService>().
                    TheDefault.Is.OfConcreteType<CookieStorageService>();




                

            }
        }
    }
}