﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Infrastructure.Logging
{
    public interface ILogger
    {
        void Log(string message);
    }
}
