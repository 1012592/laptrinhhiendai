﻿using SieuThiDienMayDoAn.Infrastructure.Querying;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Infrastructure.Domain
{
    public interface IService<T, TId> where T : IAggregateRoot
    {
        T FindBy(TId id);
        IEnumerable<T> FindAll();
        IEnumerable<T> FindAll(int index, int count);
        IEnumerable<T> FindBy(Query query);

        IEnumerable<T> FindBy(Query query, int index, int count);
        void Save(T entity);
        TId Add(T entity);
        void Remove(T entity);
    }
}
