﻿using SieuThiDienMayDoAn.Model.Customers;
using SieuThiDienMayDoAn.Services.Messaging.CustomerService;
using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Interfaces
{
    public interface ICustomerService
    {
        CreateCustomerResponse CreateCustomer(CreateCustomerRequest request);
        GetCustomerResponse GetCustomer(GetCustomerRequest request);
        ModifyCustomerResponse ModifyCustomer(ModifyCustomerRequest request);
        DeliveryAddressModifyResponse ModifyDeliveryAddress(
        DeliveryAddressModifyRequest request);
        DeliveryAddressAddResponse AddDeliveryAddress(
        DeliveryAddressAddRequest request);
        Customer FindBy(string userName);
        IEnumerable<DeliveryAddressView> GetDeliveryAddresses(int customerId);
    }
}
