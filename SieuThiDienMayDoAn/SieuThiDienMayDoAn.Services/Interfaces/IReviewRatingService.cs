﻿using SieuThiDienMayDoAn.Services.Messaging.ReviewRatingService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Interfaces
{
    public interface IReviewRatingService
    {
        GetAllReviewByProductResponse GetAllReviewByProduct(GetAllReviewByProductRequest getAllReviewByProductRequest);

        GetAllReviewByProductResponse GetAllReviewByProductWithPaging(GetAllReviewByProductRequest getAllReviewByProductRequest,int index,int count);

    }
}
