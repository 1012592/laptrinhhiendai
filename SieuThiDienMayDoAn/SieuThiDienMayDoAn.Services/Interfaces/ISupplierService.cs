﻿using SieuThiDienMayDoAn.Infrastructure.Domain;
using SieuThiDienMayDoAn.Model.Suppliers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Interfaces
{
    public interface ISupplierService : IService<Supplier, int>
    {
    }
}
