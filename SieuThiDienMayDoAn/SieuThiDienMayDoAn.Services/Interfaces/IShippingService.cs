﻿using SieuThiDienMayDoAn.Model.Shipping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Interfaces
{
    public interface IShippingService
    {
        IEnumerable<DeliveryOption> FindAll();
    }
}
