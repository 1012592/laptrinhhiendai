﻿using AutoMapper;
using SieuThiDienMayDoAn.Model.Products;
using SieuThiDienMayDoAn.Services.Messaging.ProductCatalogService;
using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Mapping
{
    public static class ProductMapper
    {
        public static ProductView ConvertToProductView(this Product product)
        {
            return Mapper.Map<Product, ProductView>(product);
        }

        public static IEnumerable<ProductSummaryView> ConvertToProductViews(
                                                  this IEnumerable<Product> products)
        {
            return Mapper.Map<IEnumerable<Product>,
                              IEnumerable<ProductSummaryView>>(products);
        }

        public static GetProductsByCategoryResponse CreateProductSearchResultFrom(this IEnumerable<Product> productsMatchingRefinement, GetProductsByCategoryRequest request)
        {
            GetProductsByCategoryResponse productSearchResultView = new GetProductsByCategoryResponse();

            IEnumerable<Product> productsFound = productsMatchingRefinement.Distinct();

            productSearchResultView.SelectedCategoryId = request.CategoryId;
            //productSearchResultView.SelectedCategoryName = 
            productSearchResultView.TotalNumberOfPages = NoOfResultPagesGiven(request.NumberOfResultsPerPage,
                                                                              productSearchResultView.NumberOfProductsFound);
            productSearchResultView.Products = CropProductListToSatisfyGivenIndex(request.Index, request.NumberOfResultsPerPage, productsFound);

            return productSearchResultView;
        }


        private static IEnumerable<ProductSummaryView> CropProductListToSatisfyGivenIndex(int pageIndex, int numberOfResultsPerPage, IEnumerable<Product> productsFound)
        {
            if (pageIndex > 1)
            {
                int numToSkip = (pageIndex - 1) * numberOfResultsPerPage;
                return productsFound.Skip(numToSkip).Take(numberOfResultsPerPage).ConvertToProductViews();
            }
            else
                return productsFound.Take(numberOfResultsPerPage).ConvertToProductViews();
        }

        private static int NoOfResultPagesGiven(int numberOfResultsPerPage, int numberOfProductsFound)
        {
            if (numberOfProductsFound < numberOfResultsPerPage)
                return 1;
            else
            {
                int noOfPages = (numberOfProductsFound / numberOfResultsPerPage);
                if (numberOfProductsFound > noOfPages * numberOfResultsPerPage)
                {
                    noOfPages += 1;
                }
                return noOfPages;
            }
        }
    }


}
