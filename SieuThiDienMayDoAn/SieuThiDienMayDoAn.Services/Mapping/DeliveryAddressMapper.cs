﻿using AutoMapper;
using SieuThiDienMayDoAn.Model.Customers;
using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Mapping
{
    public static  class DeliveryAddressMapper
    {
        public static DeliveryAddressView ConvertToDeliveryAddressView(this DeliveryAddress deliveryAddress)
        {
            return Mapper.Map<DeliveryAddress,DeliveryAddressView>(deliveryAddress);
        }

        public static IEnumerable<DeliveryAddressView> ConvertToDeliveryAddressesView(
        this IEnumerable<DeliveryAddress> deliveryAddresses)
        {
            return Mapper.Map<IEnumerable<DeliveryAddress>,
            IEnumerable<DeliveryAddressView>>(deliveryAddresses);
        }
    }
}
