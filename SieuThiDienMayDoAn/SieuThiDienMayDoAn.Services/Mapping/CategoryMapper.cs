﻿using AutoMapper;
using SieuThiDienMayDoAn.Model.Categories;
using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Mapping
{
    public static class CategoryMapper
    {
        public static IEnumerable<CategoryView> ConvertToCategoryViews(
        this IEnumerable<Category> categories)
        {
            return Mapper.Map<IEnumerable<Category>,
            IEnumerable<CategoryView>>(categories);
        }
    }
}
