﻿using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Messaging.ReviewRatingService
{
    public class GetAllReviewByProductResponse
    {
        public IEnumerable<ReviewView> ReviewViews { get; set; }
    }
}
