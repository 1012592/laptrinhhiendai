﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Messaging.ProductService
{
    public class GetProductRequest
    {
        public int ProductId { get; set; }
    }
}
