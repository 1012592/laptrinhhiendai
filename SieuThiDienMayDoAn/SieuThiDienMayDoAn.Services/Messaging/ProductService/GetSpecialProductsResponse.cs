﻿using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Messaging.ProductService
{
    public class GetSpecialProductsResponse
    {
        public IEnumerable<ProductSummaryView> Products { get; set; }
    }
}
