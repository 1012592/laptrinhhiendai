﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Messaging.CustomerService
{
    public class GetCustomerRequest
    {
        public string CustomerIdentityToken { get; set; }
        public bool LoadOrderSummary { get; set; }
    }
}
