﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.ViewModels
{
    public class ProductSummaryView
    {
       
        public int Id { get; set; }


        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public decimal Price { get; set; }

        public long Quantity { get; set; }


        public DateTime ImportDate { get; set; }

        public long SoldQuantity { get; set; }

        public int Rating { get; set; }
        public float Discount { get; set; }
    }
}
