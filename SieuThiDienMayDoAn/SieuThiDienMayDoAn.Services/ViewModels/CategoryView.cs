﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.ViewModels
{
    public class CategoryView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<ProductSummaryView> ProductSumaryViews { get; set; }
    }
}
