﻿using SieuThiDienMayDoAn.Infrastructure.Querying;
using SieuThiDienMayDoAn.Model.Products;
using SieuThiDienMayDoAn.Services.Messaging.ProductCatalogService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Implementations
{
    public class ProductSearchRequestQueryGenerator
    {
        public static Query CreateQueryFor(GetProductsByCategoryRequest getProductsByCategoryRequest)
        {
            Query productQuery = new Query();
            
            productQuery.Add(Criterion.Create<Product>(p => p.ProductCategory.Id,
                  getProductsByCategoryRequest.CategoryId, CriteriaOperator.Equal));

            return productQuery;
        }
    }
}
