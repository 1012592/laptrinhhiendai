﻿using SieuThiDienMayDoAn.Model.SuperCategories;
using SieuThiDienMayDoAn.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Implementations
{
    public class SuperCategoryService:AbstractService<SuperCategory,int>,ISuperCategoryService
       
    {
        ISuperCategoryRepository _superCategoryRepository;
        public SuperCategoryService(ISuperCategoryRepository superCategoryRepository)
            : base(superCategoryRepository)
        { _superCategoryRepository = superCategoryRepository; }
    }
}
