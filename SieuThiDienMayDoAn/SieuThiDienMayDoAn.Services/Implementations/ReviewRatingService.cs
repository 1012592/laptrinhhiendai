﻿using SieuThiDienMayDoAn.Infrastructure.Querying;
using SieuThiDienMayDoAn.Model.Categories;
using SieuThiDienMayDoAn.Model.Products;
using SieuThiDienMayDoAn.Model.Review;
using SieuThiDienMayDoAn.Services.Interfaces;
using SieuThiDienMayDoAn.Services.Messaging.ReviewRatingService;
using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SieuThiDienMayDoAn.Services.Mapping;

namespace SieuThiDienMayDoAn.Services.Implementations
{
    public class ReviewRatingService :IReviewRatingService
    {
        private readonly IProductRepository _productRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IReviewRepository _reviewRepository;


        public ReviewRatingService(
            IProductRepository productRepository,ICategoryRepository categoryRepository,IReviewRepository reviewRepository)
        {
            
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
            _reviewRepository = reviewRepository;
        }


        public GetAllReviewByProductResponse GetAllReviewByProduct(GetAllReviewByProductRequest getAllReviewByProductRequest)
        {
            GetAllReviewByProductResponse getAllReviewByProductResponse = new GetAllReviewByProductResponse();

            Query query = GetAllReviewByProductQueryGenerator.CreateQueryFor(getAllReviewByProductRequest);

            IEnumerable<ReviewView> reviewViews = _reviewRepository.FindBy(query).ConvertToReviewViews();

            getAllReviewByProductResponse.ReviewViews = reviewViews;

            return getAllReviewByProductResponse;
            
        }

        public GetAllReviewByProductResponse GetAllReviewByProductWithPaging(GetAllReviewByProductRequest getAllReviewByProductRequest, int index, int count)
        {
            GetAllReviewByProductResponse getAllReviewByProductResponse = new GetAllReviewByProductResponse();

            Query query = GetAllReviewByProductQueryGenerator.CreateQueryFor(getAllReviewByProductRequest);

            IEnumerable<ReviewView> reviewViews = _reviewRepository.FindBy(query,index,count).ConvertToReviewViews();

            getAllReviewByProductResponse.ReviewViews = reviewViews;

            return getAllReviewByProductResponse;
        }

    }
}
