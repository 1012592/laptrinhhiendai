﻿using SieuThiDienMayDoAn.Infrastructure.Domain;
using SieuThiDienMayDoAn.Infrastructure.Querying;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Implementations
{
    public abstract class AbstractService<T, TEntityKey> where T : IAggregateRoot
    {
        public AbstractService()
        {

        }

        IRepository<T, TEntityKey> _repository;

        public AbstractService(IRepository<T, TEntityKey> repository)
        {
            this._repository = repository;
        }

        public TEntityKey Add(T entity)
        {
            return _repository.Add(entity);
        }

        public void Remove(T entity)
        {
            _repository.Remove(entity);
        }

        public void Save(T entity)
        {
            _repository.Save(entity);
        }

        public T FindBy(TEntityKey id)
        {
            return _repository.FindBy(id);
        }

        public IEnumerable<T> FindAll()
        {
            return _repository.FindAll();
        }

        public IEnumerable<T> FindAll(int index, int count)
        {
            return _repository.FindAll(index, count);
        }

        public IEnumerable<T> FindBy(Query query)
        {
            return _repository.FindBy(query);


        }


        public IEnumerable<T> FindBy(Query query, int index, int count)
        {
            return _repository.FindBy(query, index, count);
        }
    }
}
