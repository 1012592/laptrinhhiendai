﻿using SieuThiDienMayDoAn.Model.Shipping;
using SieuThiDienMayDoAn.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Implementations
{
    public class ShippingService:IShippingService
    {
        private IDeliveryOptionRepository _deliveryOptionRepository;

        public ShippingService(IDeliveryOptionRepository deliveryOptionRepository)
        {
            this._deliveryOptionRepository = deliveryOptionRepository;
        }

        public IEnumerable<DeliveryOption> FindAll()
        {
            return _deliveryOptionRepository.FindAll();
        }
    }
}
