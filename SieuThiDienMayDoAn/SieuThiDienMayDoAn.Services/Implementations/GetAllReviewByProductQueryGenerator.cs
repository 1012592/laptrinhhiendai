﻿using SieuThiDienMayDoAn.Infrastructure.Querying;
using SieuThiDienMayDoAn.Model.Review;
using SieuThiDienMayDoAn.Services.Messaging.ReviewRatingService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Services.Implementations
{
    public  class GetAllReviewByProductQueryGenerator
    {
        public static Query CreateQueryFor(GetAllReviewByProductRequest getAllReviewByProductRequest)
        {
            Query query = new Query();
            query.Add(Criterion.Create<Review>(rv => rv.Product.Id, getAllReviewByProductRequest.ProductId,
                CriteriaOperator.Equal));
            return query;
        }
    }
}
