﻿using SieuThiDienMayDoAn.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model
{
    public class Address : EntityBase<int>
    {
        public string NumberOfHouse { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string Ward { get; set; }
        public string District { get; set; }
        

        protected override void Validate()
        {
            
        }
    }
}
