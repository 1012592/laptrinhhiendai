﻿using SieuThiDienMayDoAn.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.Shipping
{
    public class DeliveryOption : EntityBase<int>, IAggregateRoot, IDeliveryOption
    {
        private readonly decimal _freeDeliveryThreshold;
        private readonly decimal _cost;

        public string Name { get; set; }
        

        public DeliveryOption()
        {
        }
        public DeliveryOption(decimal freeDeliveryThreshold, decimal cost)
        {
            _freeDeliveryThreshold = freeDeliveryThreshold;
            _cost = cost;
           
        }

        public decimal GetDeliveryChargeForBasketTotalOf(decimal total)
        {
            if (total > FreeDeliveryThreshold)
            {
                return 0;
            }
            return Cost;
        }

        public decimal FreeDeliveryThreshold
        {
            get { return _freeDeliveryThreshold; }
        }
        public decimal Cost
        {
            get { return _cost; }
        }

        protected override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}
