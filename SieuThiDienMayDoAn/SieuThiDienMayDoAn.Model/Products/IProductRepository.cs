﻿using SieuThiDienMayDoAn.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.Products
{
    public interface IProductRepository : IRepository<Product, int>
    {
        IEnumerable<Product> SearchProducts(string searchStr, int index, int numberOfRecordsPerPage);
    }
}
