﻿
using SieuThiDienMayDoAn.Infrastructure.Domain;
using SieuThiDienMayDoAn.Model.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.Suppliers
{
    public class Supplier : EntityBase<int>, IAggregateRoot, IProductAttribute
    {
        public string Address { get; set; }
        public string Phone { get; set; }

        public string Name { get; set; }

        protected override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}
