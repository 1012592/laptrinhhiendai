﻿using SieuThiDienMayDoAn.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.Customers
{
    public class DeliveryAddress : Address, IAggregateRoot
    {
        
        public Customer Customer { get; set; }
        protected override void Validate()
        {
            base.Validate();
            
            if (Customer == null)
                base.AddBrokenRule(DeliveryAddressBusinessRules.CustomerRequired);
        }
    }
}
