﻿using SieuThiDienMayDoAn.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.Brands
{
    public interface IBrandRepository : IRepository<Brand, int>
    {
    }
}
