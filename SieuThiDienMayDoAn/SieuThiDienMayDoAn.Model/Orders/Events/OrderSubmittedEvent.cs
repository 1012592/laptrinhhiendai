﻿using SieuThiDienMayDoAn.Infrastructure.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.Orders.Events
{
    public class OrderSubmittedEvent:IDomainEvent
    {
        public Order Order { get; set; }
    }
}
