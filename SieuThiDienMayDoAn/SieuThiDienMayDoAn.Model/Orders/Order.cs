﻿using SieuThiDienMayDoAn.Infrastructure.Domain;
using SieuThiDienMayDoAn.Model.Customers;
using SieuThiDienMayDoAn.Model.Orders.States;
using SieuThiDienMayDoAn.Model.Products;
using SieuThiDienMayDoAn.Model.Shipping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Model.Orders
{
    public class Order : EntityBase<int>, IAggregateRoot
    {
        private IList<OrderItem> _items;
        private DateTime _created;
        
        private IOrderState _state;

        public Order()
        {
            _created = DateTime.Now;
            _items = new List<OrderItem>();
            _state = OrderStates.Open;
        }
        public DateTime Created
        {
            get { return _created; }
        }

        public IOrderState State
        {
            get { return _state; }
        }

        public decimal ShippingCharge { get; set; }
        
        public decimal ItemTotal()
        {
            return Items.Sum(i => i.LineTotal());
        }
        public decimal Total()
        {
            return Items.Sum(i => i.LineTotal()) + ShippingCharge;
        }
        
        public Customer Customer { get; set; }
        public int DeliveryAddressId { get; set; }

        public IEnumerable<OrderItem> Items
        {
            get { return _items; }
        }
        public OrderStatus Status
        {
            get { return _state.Status; }
        }

        public void AddItem(Product product, int qty)
        {
            if (_state.CanAddProduct())
            {
                if (!OrderContains(product))
                    _items.Add(OrderItemFactory.CreateItemFor(product, this, qty));
            }
            else
                throw new CannotAmendOrderException(String.Format(
                "You cannot add an item to an order with the status of '{0}'.",
                Status.ToString()));
        }

        private bool OrderContains(Product product)
        {
            return _items.Any(i => i.Contains(product));
        }


        protected override void Validate()
        {
            if (Created == DateTime.MinValue)
                base.AddBrokenRule(OrderBusinessRules.CreatedDateRequired);
            if (Customer == null)
                base.AddBrokenRule(OrderBusinessRules.CustomerRequired);
            
            if (Items == null || Items.Count() == 0)
                base.AddBrokenRule(OrderBusinessRules.ItemsRequired);
            if (Items == null || Items.Count() == 0)
                base.AddBrokenRule(OrderBusinessRules.ItemsRequired);
            
        }

        internal void SetStateTo(IOrderState state)
        {
            this._state = state;
        }

        public override string ToString()
        {
            StringBuilder orderInfo = new StringBuilder();
            foreach (OrderItem item in _items)
            {
                orderInfo.AppendLine(String.Format("{0} of {1}",
                item.Qty, item.Product.Name));
            }
            orderInfo.AppendLine(String.Format("Shipping: {0}", this.ShippingCharge));
            orderInfo.AppendLine(String.Format("Total: {0}", this.Total()));
            return orderInfo.ToString();
        }
    }
}
