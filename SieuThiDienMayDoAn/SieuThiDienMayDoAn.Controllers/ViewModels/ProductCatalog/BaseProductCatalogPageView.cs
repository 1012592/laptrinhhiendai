﻿using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Controllers.ViewModels.ProductCatalog
{
    public abstract class BaseProductCatalogPageView : BasePageView
    {
        public IEnumerable<CategoryView> Categories { get; set; }
        public IEnumerable<SuperCategoryView> SuperCategories { get; set; }
    }
}
