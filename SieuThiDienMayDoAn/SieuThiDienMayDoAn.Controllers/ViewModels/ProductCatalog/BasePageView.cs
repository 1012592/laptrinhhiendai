﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Controllers.ViewModels.ProductCatalog
{
    public abstract class BasePageView
    {
        public BasketSummaryView BasketSummary { get; set; }
    }
}
