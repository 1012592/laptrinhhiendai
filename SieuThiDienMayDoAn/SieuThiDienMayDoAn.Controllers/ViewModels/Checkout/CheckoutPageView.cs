﻿using SieuThiDienMayDoAn.Controllers.ViewModels.ProductCatalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Controllers.ViewModels.Checkout
{
    public class CheckoutPageView : BaseProductCatalogPageView
    {
        public Guid BasketId { get; set; }
    }
}
