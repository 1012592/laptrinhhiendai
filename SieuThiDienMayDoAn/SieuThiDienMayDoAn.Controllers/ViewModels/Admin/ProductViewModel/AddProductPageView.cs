﻿using SieuThiDienMayDoAn.Model.Brands;
using SieuThiDienMayDoAn.Model.Categories;
using SieuThiDienMayDoAn.Model.Suppliers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Controllers.ViewModels.Admin.ProductViewModel
{
    public class AddProductPageView
    {
        public IEnumerable<Brand> Brands { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<Supplier> Suppliers { get; set; }
    }
}
