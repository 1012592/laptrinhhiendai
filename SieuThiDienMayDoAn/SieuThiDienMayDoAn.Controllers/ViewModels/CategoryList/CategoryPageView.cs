﻿using SieuThiDienMayDoAn.Controllers.ViewModels.ProductCatalog;
using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Controllers.ViewModels.CategoryList
{
    public class CategoryPageView:BaseProductCatalogPageView
    {
        public CategoryView CategoryView { get; set; }
    }
}
