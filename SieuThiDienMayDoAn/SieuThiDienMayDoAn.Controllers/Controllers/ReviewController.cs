﻿using SieuThiDienMayDoAn.Controllers.ViewModels.Review;
using SieuThiDienMayDoAn.Infrastructure.CookieStorage;
using SieuThiDienMayDoAn.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SieuThiDienMayDoAn.Controllers.Controllers
{
    public class ReviewController : ProductCatalogBaseController
    {
        private readonly IProductCatalogService _productCatalogService;

        private readonly IProductService _productService;
        private readonly ISuperCategoryService _superCategoryService;
        private readonly ICategoryService _categoryService;

        public ReviewController(ICookieStorageService cookieStorageService, IProductCatalogService productCatalogService, ICategoryService categoryService, IProductService productService, ISuperCategoryService superCategoryService)
            : base(cookieStorageService, categoryService, superCategoryService)
        {
            _productCatalogService = productCatalogService;

            _categoryService = categoryService;
            _productService = productService;
            _superCategoryService = superCategoryService;
        }

        public ActionResult ListReview()
        {
            ListReviewPage listReviewPage = new ListReviewPage();
            listReviewPage.BasketSummary = base.GetBasketSummaryView();
            listReviewPage.Categories = base.GetCategories();
            listReviewPage.SuperCategories = base.GetSuperCategories();
            return View(listReviewPage);
        }

    }
}
