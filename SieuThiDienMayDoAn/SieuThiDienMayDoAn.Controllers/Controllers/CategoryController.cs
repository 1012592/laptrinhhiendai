﻿using SieuThiDienMayDoAn.Controllers.ViewModels.CategoryList;
using SieuThiDienMayDoAn.Infrastructure.Configuration;
using SieuThiDienMayDoAn.Infrastructure.CookieStorage;
using SieuThiDienMayDoAn.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SieuThiDienMayDoAn.Controllers.Controllers
{
    public class CategoryController:ProductCatalogBaseController
    {
        private readonly IProductCatalogService _productCatalogService;
        
        private readonly ISuperCategoryService _superCategoryService;
        private readonly ICategoryService _categoryService;
        public CategoryController(ICookieStorageService cookieStorageService,IProductCatalogService productCatalogService, ISuperCategoryService superCategoryService,ICategoryService categoryService)
            : base(cookieStorageService,categoryService, superCategoryService)
        {
            _productCatalogService = productCatalogService;
            _categoryService = categoryService;
            _superCategoryService = superCategoryService;
        }

        
    }
}
