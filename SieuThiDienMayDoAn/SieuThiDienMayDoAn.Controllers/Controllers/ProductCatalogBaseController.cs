﻿using SieuThiDienMayDoAn.Services.Interfaces;
using SieuThiDienMayDoAn.Services.Messaging.ProductCatalogService;

using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SieuThiDienMayDoAn.Services.Mapping;
using SieuThiDienMayDoAn.Infrastructure.CookieStorage;

namespace SieuThiDienMayDoAn.Controllers.Controllers
{
    public class ProductCatalogBaseController : BaseController
    {
       
        private readonly ISuperCategoryService _superCategoryService;
        private readonly ICategoryService _categoryService;

        public ProductCatalogBaseController(ICookieStorageService cookieStorageService, ICategoryService categoryService, ISuperCategoryService superCategoryService)
            : base(cookieStorageService)
        {
            _categoryService =categoryService;
            _superCategoryService = superCategoryService;
        }

        public IEnumerable<CategoryView> GetCategories()
        {
            return _categoryService.FindAll().ConvertToCategoryViews();
        }

        public IEnumerable<SuperCategoryView> GetSuperCategories()
        {

            return _superCategoryService.FindAll().ConvertToSuperCategoryViews();
        }


    }
}
