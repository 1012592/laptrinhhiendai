﻿using SieuThiDienMayDoAn.Controllers.JsonDTOs;
using SieuThiDienMayDoAn.Controllers.ViewModels.ProductCatalog;
using SieuThiDienMayDoAn.Infrastructure.Configuration;
using SieuThiDienMayDoAn.Infrastructure.CookieStorage;
using SieuThiDienMayDoAn.Services.Interfaces;
using SieuThiDienMayDoAn.Services.Messaging.ProductCatalogService;
using SieuThiDienMayDoAn.Services.Messaging.ProductService;
using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SieuThiDienMayDoAn.Controllers.Controllers
{
    public class ProductController : ProductCatalogBaseController
    {
        private readonly IProductCatalogService _productCatalogService;
        private readonly ISuperCategoryService _supercategoryService;
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;

        public ProductController(ICookieStorageService cookieStorageService,IProductCatalogService productCatalogService,ISuperCategoryService superCategoryService,ICategoryService categoryService,IProductService productService)
            : base(cookieStorageService,categoryService, superCategoryService)
        {
            _productCatalogService = productCatalogService;
            _supercategoryService = superCategoryService;
            _categoryService = categoryService;
            _productService = productService;
        }

        public ActionResult GetProductsByCategory(int categoryId)
        {
            ProductsByCategoryView productsByCategoryView = new ProductsByCategoryView();
            productsByCategoryView.Categories = base.GetCategories();
            productsByCategoryView.SuperCategories = base.GetSuperCategories();
            productsByCategoryView.BasketSummary = base.GetBasketSummaryView();
            

            GetProductsByCategoryRequest productSearchRequest = GenerateInitialProductSearchRequestFrom(categoryId);
            GetProductsByCategoryResponse response =
            _productCatalogService.GetProductsByCategory(productSearchRequest);
            ProductsByCategoryResultView productsByCategoryResultView = GetProductsByCategoryResultViewFrom(response);


            return View("ProductsByCategoryResult", productsByCategoryView);
        }

        private ProductsByCategoryResultView GetProductsByCategoryResultViewFrom(GetProductsByCategoryResponse response)
        {
            ProductsByCategoryResultView productsByCategoryResultView =
            new ProductsByCategoryResultView();
            productsByCategoryResultView.Categories = base.GetCategories();
            productsByCategoryResultView.SuperCategories = base.GetSuperCategories();

            productsByCategoryResultView.CurrentPage = response.CurrentPage;
            //productSearchResultView.NumberOfTitlesFound =

            productsByCategoryResultView.SelectedCategoryId = response.SelectedCategoryId;
            productsByCategoryResultView.SelectedCategoryName =response.SelectedCategoryName;
            productsByCategoryResultView.TotalNumberOfPages =response.TotalNumberOfPages;
            return productsByCategoryResultView;
        }



        private static GetProductsByCategoryRequest GenerateInitialProductSearchRequestFrom(int categoryId)
        {
            GetProductsByCategoryRequest productSearchRequest =new GetProductsByCategoryRequest();
            productSearchRequest.NumberOfResultsPerPage = int.Parse(
            ApplicationSettingsFactory
            .GetApplicationSettings().NumberOfResultsPerPage);
            productSearchRequest.CategoryId = categoryId;
            productSearchRequest.Index = 1;
            productSearchRequest.SortBy = ProductsSortBy.PriceHighToLow;
            return productSearchRequest;
        }

        public ActionResult Detail(int id)
        {
            ProductDetailView productDetailView = new ProductDetailView();
            GetProductRequest request = new GetProductRequest() { ProductId = id };
            GetProductResponse response = _productService.GetProduct(request);
            ProductView productView = response.Product;
            productDetailView.Product = productView;
            productDetailView.BasketSummary = base.GetBasketSummaryView();
            productDetailView.Categories = base.GetCategories();
            productDetailView.SuperCategories = base.GetSuperCategories();
            return View(productDetailView);
        }

        public ActionResult SearchProducts(string searchStr)
        {
            ViewBag.SearchStr = searchStr;
            SearchProductsView searchProductsView = new SearchProductsView();
            searchProductsView.Categories = base.GetCategories();
            searchProductsView.SuperCategories = base.GetSuperCategories();
            searchProductsView.BasketSummary = base.GetBasketSummaryView();

            return View(searchProductsView);
        }

    }
}
