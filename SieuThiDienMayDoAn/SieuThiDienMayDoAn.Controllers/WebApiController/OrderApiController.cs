﻿using SieuThiDienMayDoAn.Services.Interfaces;
using SieuThiDienMayDoAn.Services.Messaging.OrderService;
using SieuThiDienMayDoAn.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace SieuThiDienMayDoAn.Controllers.WebApiController
{
    [Authorize]
    public class OrderApiController:ApiController
    {
        IOrderService _orderService;
        public OrderApiController(IOrderService orderService)
        {
            this._orderService = orderService;
        }

        [HttpPost]
        public CreateOrderResponse CreateOrder(CreateOrderRequest createOrderRequest)
        {

            CreateOrderResponse createOrderResponse =
             _orderService.CreateOrder(createOrderRequest);

            return createOrderResponse;
        }
    }
}
