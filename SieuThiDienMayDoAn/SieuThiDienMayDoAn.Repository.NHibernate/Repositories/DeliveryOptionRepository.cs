﻿using SieuThiDienMayDoAn.Infrastructure.UnitOfWork;
using SieuThiDienMayDoAn.Model.Shipping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Repository.NHibernate.Repositories
{
    public class DeliveryOptionRepository: Repository<DeliveryOption, int>,IDeliveryOptionRepository
    {
        public DeliveryOptionRepository(IUnitOfWork uow)
            : base(uow)
        {
        }
    }
}
