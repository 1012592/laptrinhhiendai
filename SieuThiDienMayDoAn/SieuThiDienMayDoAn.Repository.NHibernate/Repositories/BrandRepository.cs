﻿using SieuThiDienMayDoAn.Infrastructure.UnitOfWork;
using SieuThiDienMayDoAn.Model.Brands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Repository.NHibernate.Repositories
{
    public class BrandRepository : Repository<Brand, int>, IBrandRepository
    {
        public BrandRepository(IUnitOfWork uow)
            : base(uow)
        {
        }
    }
}
