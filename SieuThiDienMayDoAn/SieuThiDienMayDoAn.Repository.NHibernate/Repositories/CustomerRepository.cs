﻿using NHibernate;
using NHibernate.Criterion;
using SieuThiDienMayDoAn.Infrastructure.Querying;
using SieuThiDienMayDoAn.Infrastructure.UnitOfWork;
using SieuThiDienMayDoAn.Model.Customers;
using SieuThiDienMayDoAn.Repository.NHibernate.SessionStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Repository.NHibernate.Repositories
{
    public class CustomerRepository : Repository<Customer, int>,ICustomerRepository
    {
        public CustomerRepository(IUnitOfWork uow)
            : base(uow)
        { 
        }

        public Customer FindBy(string identityToken)
        {
            ICriteria criteriaQuery = SessionFactory.GetCurrentSession()
            .CreateCriteria(typeof(Customer))
            .Add(Expression.Eq(PropertyNameHelper.ResolvePropertyName<Customer>(c => c.UserName), identityToken));
            IList<Customer> customers = criteriaQuery.List<Customer>();
            Customer customer = customers.FirstOrDefault();
            return customer;
        }
    }
}
