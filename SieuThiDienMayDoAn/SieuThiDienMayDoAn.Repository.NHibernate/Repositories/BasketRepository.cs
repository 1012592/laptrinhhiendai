﻿using SieuThiDienMayDoAn.Infrastructure.UnitOfWork;
using SieuThiDienMayDoAn.Model.Basket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Repository.NHibernate.Repositories
{
    public class BasketRepository : Repository<Basket, Guid>, IBasketRepository
    {
        public BasketRepository(IUnitOfWork uow)
            : base(uow)
        {
        }
    }
}
