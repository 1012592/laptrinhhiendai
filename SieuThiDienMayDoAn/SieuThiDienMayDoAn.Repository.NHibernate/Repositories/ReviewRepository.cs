﻿using NHibernate;
using SieuThiDienMayDoAn.Infrastructure.UnitOfWork;
using SieuThiDienMayDoAn.Model.Review;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Repository.NHibernate.Repositories
{
    public class ReviewRepository :Repository<Review, int>, IReviewRepository
    {
        public ReviewRepository(IUnitOfWork uow)
            : base(uow)
        {
        }

        public override void AppendCriteria(ICriteria criteria)
        {
            criteria.CreateAlias("Product", "product");


        }
    }
}
