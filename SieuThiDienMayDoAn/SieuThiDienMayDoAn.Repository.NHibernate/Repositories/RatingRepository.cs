﻿using SieuThiDienMayDoAn.Infrastructure.UnitOfWork;
using SieuThiDienMayDoAn.Model.Review;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Repository.NHibernate.Repositories
{
    public class RatingRepository : Repository<Rating, int>, IRatingRepository
    {
        public RatingRepository(IUnitOfWork uow)
            : base(uow)
        {
        }

    }
}
