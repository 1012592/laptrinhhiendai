﻿using SieuThiDienMayDoAn.Infrastructure.UnitOfWork;
using SieuThiDienMayDoAn.Model.Review;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieuThiDienMayDoAn.Repository.NHibernate.Repositories
{
    public class CommentRepository: Repository<Comment, int>, ICommentRepository
    {
        public CommentRepository(IUnitOfWork uow)
            : base(uow)
        {
        }
    }
}
